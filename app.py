from flask import Flask
from flask import Response
from flask import render_template
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def index():
    #return render_template("index.html")
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("Troia Hello at " + current_time + "!")
    return("Troia Hello at " + current_time + "!")


@app.route('/test')
def test():
    print("Hello from test!")
    return("Hello from test!")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8080')
